Click==7.0
Django==2.2.3
django-appconf==1.0.3
django-compressor==2.3
django-sass-processor==0.7.3
django-anymail==7.0.0
getfiles==2019.4.13
libsass==0.19.2
load==2019.4.13
markdown-table==2019.4.13
mkdir==2019.4.13
orderdict==2019.4.13
psycopg2==2.8.3
public==2019.4.13
pytz==2019.1
rcssmin==1.0.6
readme-docstring==2019.4.13
readme-generator==2019.4.13
rjsmin==1.1.0
setupcfg==2019.4.13
six==1.12.0
sqlparse==0.3.0
uWSGI==2.0.18
values==2019.4.13
write==2019.4.13
