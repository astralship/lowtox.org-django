from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist, FieldDoesNotExist
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.core.mail import send_mail
from django.utils.crypto import get_random_string

from django.apps import apps



def index_view(request):
    return render(request, 'mainwebsite/index.html')


def intro_view(request):
    return render(request, 'mainwebsite/intro.html')


def who_can_apply_view(request):
    return render(request, 'mainwebsite/who-can-apply.html')


def financial_support_view(request):
    return render(request, 'mainwebsite/financial-support.html')


def tasks_view(request):
    return render(request, 'mainwebsite/tasks.html')


def faq_view(request):
    return render(request, 'mainwebsite/faq.html')


def apply_view(request):
    if request.method == 'GET':
        return render(request, 'mainwebsite/apply.html')
    elif request.method == 'POST':
        email = request.POST.get("email")
        if email:
            token = get_new_token(email);
            # send_mail("Lowtox.org Login Link",
            #           "Thank you for you interest in the lowtox.org project. "
            #           "Here is your personal login link, Do not share it. You can request a "
            #           "new link anytime by entering your email address on the \"apply\" page. "
            #           "Your login link is : http://localhost:8000/accounts/token/" + token,
            #           "william@jedrzejak.fr",
            #           [email])
            print("http://localhost:8000/accounts/token/" + token)
            url = "%s?message=check_email" % reverse('mainwebsite:apply')
            return HttpResponseRedirect(url)


def get_new_token(email):
    try:
        user = User.objects.get(email=email)
    except ObjectDoesNotExist:
        # Create a new user.
        user = User(username=email, email=email)
        user.save()

    # Create a new unique token
    Token = apps.get_model('accounts', 'Token')
    tk = get_random_string(length=100)
    while Token.objects.filter(token=tk):
        tk = get_random_string(length=100)

    # Save and return the new token
    if hasattr(user, 'token'):
        user.token.token = tk
    else:
        user.token = Token(user=user, token=tk)
    user.token.save()
    return tk;
