from django.urls import path, include
from . import views

app_name = 'mainwebsite'
urlpatterns = [
    path('', views.index_view, name='index'),
    path('intro', views.intro_view, name='intro'),
    path('who-can-apply', views.who_can_apply_view, name='who-can-apply'),
    path('financial-support', views.financial_support_view, name='financial-support'),
    path('tasks', views.tasks_view, name='tasks'),
    path('faq', views.faq_view, name='faq'),
    path('apply', views.apply_view, name='apply'),

]
