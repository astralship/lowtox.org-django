from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy, reverse
from django.views import generic

from django.conf import settings
from .forms import SignUpForm


class SignUp(generic.CreateView):
    form_class = SignUpForm
    success_url = reverse_lazy('login')
    template_name = 'accounts/signup.html'


def login_token(request, token):
    user = authenticate(token=token)
    if user is not None:
        login(request, user)
        if settings.LOGIN_REDIRECT_URL:
            return HttpResponseRedirect(reverse(settings.LOGIN_REDIRECT_URL))
        else:
            return HttpResponseRedirect(reverse('mainwebsite:index'))
    else:
        return HttpResponseRedirect(reverse('mainwebsite:apply'))
