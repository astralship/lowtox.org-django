from django.contrib.auth.models import User
from django.db import models


class Token(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)
    token = models.CharField(max_length=100, unique=True)
