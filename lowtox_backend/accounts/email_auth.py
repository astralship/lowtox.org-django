from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist


class EmailAuthBackend:
    """
    Authenticate against a token
    """

    def authenticate(self, request, token=None):
        try:
            user = User.objects.get(token__token=token)
        except ObjectDoesNotExist:
            return None
        return user


    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
