# Generated by Django 2.2.3 on 2019-07-31 10:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0018_answer_skiped'),
    ]

    operations = [
        migrations.RenameField(
            model_name='answer',
            old_name='skiped',
            new_name='skipped',
        ),
    ]
