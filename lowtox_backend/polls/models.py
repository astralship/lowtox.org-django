from django.contrib.auth.models import User
from django.db import models
from django.db.models import UniqueConstraint


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    label = models.CharField(max_length=200, blank=True, null=True, default=None)  # Label for number & text questions
    NUMBER = 'NB'
    TEXT = 'TXT'
    ONE_ANSWER = 'ONE'
    MULTIPLE_ANSWER = 'MULTI'
    QUESTIONS_TYPES = [
        (NUMBER, 'Number'),
        (TEXT, 'Text'),
        (ONE_ANSWER, 'One Answer'),
        (MULTIPLE_ANSWER, 'Multiple Answers'),
    ]
    type = models.CharField(max_length=5, choices=QUESTIONS_TYPES, default=ONE_ANSWER)

    def __str__(self):
        return self.question_text

    def is_text(self):
        return self.type == self.TEXT

    def is_number(self):
        return self.type == self.NUMBER

    def is_one_answer(self):
        return self.type == self.ONE_ANSWER

    def is_multiple_answer(self):
        return self.type == self.MULTIPLE_ANSWER


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.choice_text


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    answer_open = models.CharField(max_length=1000)
    choice = models.ManyToManyField(Choice)
    skipped = models.BooleanField(default=False)

    class Meta:
        constraints = [
            UniqueConstraint(fields=['question', 'user'], name='unique answer')
        ]

    def __str__(self):
        if self.question.is_number() or self.question.is_text():
            return self.answer_open
        else:
            return ''

    def get_answer(self):
        if self.skipped:
            return 'Skipped'
        if self.question.is_text() or self.question.is_number():
            return self.answer_open
        elif self.question.is_one_answer() or self.question.is_multiple_answer():
            return [c.choice_text for c in self.choice.all()]


class UserData(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    question = models.ForeignKey(
        Question,
        on_delete=models.CASCADE,
        null=True,
    )
