<img src="https://gitlab.com/astralship/lowtox.org-django/wikis/uploads/74b90c5ac22f087f3c919cd7969d8e69/logo.png"  width="300">


# Lowtox.org

> Lowtox.org website writen in python using the django framework

<img src="https://gitlab.com/astralship/lowtox.org-django/wikis/uploads/de62fc85f27723e94de1f90716f6b291/Screenshot_20190830_095526.png">

---

## Installation

run 
```shell
./install --local
```

to install the project to /opt and start services. 

## Features

The lowtox.org website is composed of :
- A website describing the project accessible by everyone. Under the "mainwebsite" folder
- A questionnaire application accessible to registered users. Under the "polls" folder
- An admin panel to add questions to the questionnaire and browse the answers.

The login process uses transactionals emails with a token system instead of passwords.



## Team

|<a href="https://gitlab.com/astralliam" target="_blank">**AstralLiam**</a>| <a href="https://gitlab.com/TheWardow" target="_blank">**TheWardow**</a> |<a href="https://gitlab.com/mirsal" target="_blank">**Mirsal**</a> |<a href="https://gitlab.com/nomoreweirdmachines" target="_blank">**Lee Hughes**</a> |
|:--:| :---: |:---:| :---:|
|<img src="https://pbs.twimg.com/profile_images/1014937474773200901/GWMAhl67.jpg" width="200">| <img src="https://secure.gravatar.com/avatar/0f066eb7a6fe3a1037a42460e96563d0?s=800&d=identicon" width="200">|<img src="https://secure.gravatar.com/avatar/b02943b092cf21a9b62e0af94f2cf86e?s=800&d=identicon" width="200"> | <img src="https://secure.gravatar.com/avatar/483f2b4a34b7c1ab2a3f1f193306642b?s=800&d=identicon" width="200">|
|<a href="https://gitlab.com/astralliam" target="_blank">`gitlab.com/astralliam`</a> | <a href="https://gitlab.com/TheWardow" target="_blank">`gitlab.com/TheWardow`</a> |  <a href="https://gitlab.com/mirsal" target="_blank">`gitlab.com/mirsal`</a>| <a href="https://gitlab.com/nomoreweirdmachines" target="_blank">`gitlab.com/nomoreweirdmachines`</a> |
---
- Copyright 2019 © <a href="https://astralship.org/" target="_blank">AstralShip</a>.
