#FROM httpd:2.4
FROM debian:buster
RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -y --no-install-recommends apt-utils pwgen
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y build-essential
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y python3-dev 
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y python3-pip 
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y libpq-dev
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y apache2 apache2-dev
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y python-virtualenv
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install supervisor && \
  mkdir -p /var/log/supervisor && \
  mkdir -p /etc/supervisor/conf.d

RUN mkdir /var/www/lowtox_backend
#RUN mkdir /usr/local/apache2/htdocs/lowtox_backend

COPY mod_wsgi-4.6.5.tar.gz /tmp/
WORKDIR /tmp
RUN tar xvfz mod_wsgi-4.6.5.tar.gz
WORKDIR mod_wsgi-4.6.5
RUN ./configure --with-python=/usr/bin/python3
RUN make 
RUN make install 

ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE lowtox_backend.settings_docker_prod
WORKDIR /var/www/lowtox_backend
COPY requirements.txt ./
RUN virtualenv venv -p /usr/bin/python3
#RUN virtualenv venv
ENV PATH="/var/www/lowtox_backend/venv/bin:$PATH"
RUN pip install -r requirements.txt

WORKDIR /var/www/lowtox_backend
COPY lowtox_backend ./lowtox_backend
RUN rm -r /etc/apache2
COPY apache2-config /etc/apache2
#COPY apache2-config/apache2.conf /usr/local/apache2/conf/httpd.conf
#COPY apache2-config/ /usr/local/apache2/conf/
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 10
#RUN /etc/init.d/apache2 start

RUN pwgen -s 32 1 > /etc/secret_key.txt
RUN python lowtox_backend/manage.py collectstatic --noinput 

RUN chown www-data:www-data -R /var/www
ADD supervisor.conf /etc/supervisor.conf
CMD ["supervisord", "-c", "/etc/supervisor.conf"]
